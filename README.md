title

:   Species

subtitle

:   A web platform to find unkown relations hidden in millions of
    species occurrence records

------------------------------------------------------------------------

Team member(s) names, roles and affiliations
============================================

Application Development Team
----------------------------

-   Raúl Sierra Alcocer, Eco-informatics Coordinator, CONABIO.
-   Juan M Barrios, Systems Architect, CONABIO.
-   Juan Carlos Salazar-Carrillo, Web Development Consultant, CONABIO.
-   Pedro Romero Martinez, Back-end Developer, CONABIO.

Scientific advisors
-------------------

-   Christopher R. Stephens, Data Science Coordinator, C3, UNAM.
-   Constantino González-Salazar, Professor-Researcher, UAM.

R package developer
-------------------

-   Enrique del Callejo Canal, PhD Student, IIMAS, UNAM.

Abstract and rationale
======================

SPECIES is a spatial data mining web platform for the exploratation of
biodiversity data. It has two models for analysis: ecological niche and
ecological networks, both constructed from spatial correlations found in
the databases. SPECIES integrates: data, a statistical engine, and an
interactive visualization front end. Combined, these components provide
an analysis tool that may guide ecologists toward new insights. SPECIES
is optimized to support fast hypotheses prototyping and testing,
analysing thousands of biotic and abiotic variables and presenting
descriptive results to the user at different levels of detail. The
rationale of this project is that, as of today, we can only explore GBIF
occurrence records with questions that are explicit on the data fields,
like, which are the records for *Panthera onca*, or which are records
from Mexico, or from a specific project. Questions like \'which mammals
are usually found near the jaguar?\', however, are not possible. We
believe that the possibility of answering that type of questions opens
up a new way of generating knowledge from GBIF and other, similar,
biodiversity data repositories. We believe that given the size of GBIF
data, which recently reached the 1 billion records, we need more tools
that can extract patterns to help our communities to better exploit this
world of information.

Operating instructions
======================

Ecological Niche
----------------

Welcome to the Ecological Niche analysis section of SPECIES. An analysis
worflow has the following steps.

1.  

    Target species configuration

    :   1.  Choose a species: type the scientific name of the target
            species. As you write the system will give options. Click on
            the name you're looking for when it appears.
        2.  Region: Choose the region that you'll use for your analysis,
            currently we have three countries (Colombia, Mexico and the
            USA) and one combined region (Mexico and the USA).
        3.  Grid resolution: Choose the grid resolution that you'll use
            for your analysis, the grid defines the spatial partition to
            define co-occurrences of variables.
        4.  Filter by date: Use the dates slider to define the time
            range of the records you want to include in your analysis.
        5.  Include data that has no date: You can include data that has
            no associated observation date. The default is that they are
            included.
        6.  Include fossil data: You can exclude fossil records of the
            chosen species. The default is that they are included.
        7.  Histogram of observations: This histogram shows the
            occurrence records frequency by year. It updates when you
            modify the time interval with the slider.
        8.  Apply filters: Once you have defined the filters for your
            analysis press this button to update the selection and the
            map.
        9.  Occurrence map: In the first map you see records that will
            be considered for your analysis. Click on an occurrence to
            display its associated data.

2.  

    Analysis configuration

    :   1.  Variable group: The next step is to define the covariates
            (niche predictors) that you will use in the model. In
            SPECIES you add them as groups of variables, you can add as
            many groups as you like.
        2.  To add groups of species as covariates, choose the taxonomic
            level of the group of species you want to define and type
            the name of the taxon. For example, if you want to add all
            mammals, select the taxonomic level \'Class\', and then type
            \'Mammalia\'. Once the taxon is chosen the system shows the
            taxonomic subtree associated with the chosen taxon where you
            can choose to check/uncheck subgroups in order to include
            them or not in the analysis. Click on the plus sign button
            to create the covariates group.
        3.  For raster data you will see a list of data sources, each
            data source contains a set of variables from that source.
            Choose the data source(s), and add them using the plus sign
            button, as in the taxa tab. You create multiple raster
            groups in this way
        4.  Create variable group: Once you have chosen a variable group
            you should add them by clicking on the plus sign button.
        5.  Parameters for niche analysis: You can adjust some
            parameters for the niche analysis.
        6.  Minimum number of occurrences: Fixes the minimum number of
            cells that a covariate occupies to be included in the
            analysis.
        7.  Run the analysis: Execute niche analysis with the chosen
            parameters.

Ecological Community
--------------------

Welcome to the Ecological Community analysis section of SPECIES. Follow
the steps to build a correlations network.

1.  Source variable group: The first step is to define source nodes that
    you will use in the model. Add the node groups one by one.
2.  Target variable group: The next step is to define target nodes.
3.  

    Variable types: In both groups, you can choose biotic and/or abiotic variables. Click on the corresponding tab of the variables you want to include.

    :   1.  Taxonomic order: For biotic variables choose the taxonomic
            level of the group of species you want to define and write
            the name of the taxon. For example, Class: Mammals.
        2.  Taxonomic tree: Once the taxon is chosen the system shows
            the taxonomic subtree associated with the chosen taxon where
            you can choose species from the subgroup.
        3.  Raster Variables: Choose the following raster variables,
            they were processed to be used as covariates in your
            analysis.

4.  Create variable group: Once you have chosen a variable group you
    should add them by clicking on plus icon button.
5.  Minimum number of occurrences: Fixes the minimum number of cells
    that a variable should cover to be included in the analysis.
6.  Grid resolution: Choose the grid resolution that you'll use for your
    analysis, the grid defines the spatial partition to define
    co-occurrences of variables.
7.  Region: Choose the region that you'll use for your analysis.
8.  Run the analysis: Execute niche analysis with the chosen parameters.

Links to visuals
================

Link(s) to submission materials
===============================

-   Website: [Species](http://species.conabio.gob.mx/dbdev/).
-   [SPECIES FrontEnd](https://bitbucket.org/conabio_c3/species-front/)
    Source code for the app FrontEnd.
-   [SNIB Middleware](https://bitbucket.org/conabio_c3/snib-middleware/)
    API service for SPECIES app
-   [SPECIES DB Build](https://bitbucket.org/conabio_c3/speciesdbbuild/)
    Scripts to build the SPECIES DB architecture
-   [Update Niche](https://bitbucket.org/conabio_c3/updateniche/) Django
    app to update SPECIES DB.

License
=======

All project components, SPECIES frontend, SNIB middleware, and
speciesDBBuild, are licensed using the [GNU Affero General Public
License v3.0](https://www.gnu.org/licenses/agpl.html).
